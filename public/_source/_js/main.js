$('html').removeClass('no-js').addClass('js');

$('#menu-btn').on('click',function(e) {
	e.preventDefault();
	$('#main-nav').slideToggle();
	$('[role=banner').toggleClass('menu-open');
	$(this).text($(this).text() == 'Menu' ? 'Close' : 'Menu');
});

function masonryLayout ( $, window ) {
	window.watchResize = function( callback )
	{
		var resizing;
		function done()
		{
			clearTimeout( resizing );
			resizing = null;
			callback();
		}
		$(window).resize(function(){
			if ( resizing )
			{
				clearTimeout( resizing );
				resizing = null;
			}
			resizing = setTimeout( done, 50 );
		});
		// init
		callback();
	};
	window.watchResize(function() {
		var size = $(window).width();

		if ( size >= 724 )
		{
			if ( !$('.listing-module').hasClass('masonry') )
			{
				$('.listing-module').addClass('masonry');
				var $container = $('.listing').masonry({
					itemSelector:'.masonry-cell',
					columnWidth: '.list-item',
					percentPostion: true
				});

				$container.imagesLoaded().progress( function() {
					$container.masonry('layout');
				});
			}

		}
	});
}
if ( $('body').hasClass('homepage') || $('body').hasClass('template-2') )
{
	masonryLayout( jQuery, window );
}

if ( $('body').hasClass('homepage') || $('body').hasClass('template-2') )
{
	$('#carousel ol').slick({
		slide:'#carousel .slide',
		infinite:true,
		slidesToShow:1,
		slidestoScroll:1,
		arrows: false,
		dots:true,
		autoplay:true,
		autoplaySpeed:5000,
		swipe:true,
		mobileFirst: true,
		responsive: [
			{
				breakpoint: 960,
				settings: {
					slidesToShow:1,
					arrows:true,
					dots:false,
				}
			}
		]
	});
}