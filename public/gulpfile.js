"use strict";

var gulp = require('gulp'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    maps = require('gulp-sourcemaps');

gulp.task("minifyScripts", function() {
    return gulp.src("_source/_js/main.js")
    .pipe(uglify())
    .pipe(rename('main.min.js'))
    .pipe(gulp.dest('_js'));
});

gulp.task('compileSass', function() {
  return gulp.src("_source/_scss/*.scss")
      .pipe(maps.init())
      .pipe(sass())
      .pipe(maps.write('./'))
      .pipe(gulp.dest('_css'));
});

gulp.task('watchSass', function() {
  gulp.watch('_source/_scss/**/*.scss', ['compileSass']);
})

gulp.task("build", ['minifyScripts', 'compileSass']);

gulp.task("default", ["build"]);
